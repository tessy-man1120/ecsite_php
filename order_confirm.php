<?php
  session_start();
  require_once "connect_database.php";
  require_once "validation/login_validation.php";
  $loginValidation->RedirectLogin();
  $connectDb->ConnectDatabase($db);
  $connectDb->SelectUserAddress($db, $address);

  if(empty($_SESSION["cart"])){
    header("Location: product_list.php");
  }

  if(isset($_POST["complete"])){
    if(empty($_POST["address"]) && empty($_POST["checkAddress"])){
      echo "[※ERROR]配送先を入力してください";
    }elseif(empty($_POST["payment"])){
      echo "[※ERROR]支払い方法を選択してください";
    }else{
      $_SESSION["productId"] = [];
      $_SESSION["cartInCount"] = [];
      $_SESSION["payment"] = $_POST["payment"];
      $_SESSION["ttlPrice"] = $_POST["ttlPrice"];
      if(!empty($_POST["address"])){
        $_SESSION["sendAddress"] = $_POST["pref"].$_POST["city"].$_POST["address"];
      }elseif(!empty($_POST["checkAddress"])){
        $_SESSION["sendAddress"] = $address;
      }
      for($i=0; $i < count($_SESSION["cart"]); $i++){
        $_SESSION["productId"][] = $_POST["productId"][$i];
        $_SESSION["cartInCount"][] = $_POST["cartInCount"][$i];
      }
      header("Location: order_complete.php");
    }
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="sample.js"></script>
  <title>購入確認</title>
</head>
<body>
  <h1>購入確認画面</h1>
  <form action='' method='POST'>
  <?php
    $ttlCartProducts = 0;
    $ttlCartPrice = 0;
    foreach($_SESSION["cart"] as $index => $product){
      $productId = $product["id"];
      $productCount = $product["count"];
      $connectDb->SelectCartProducts($db, $products, $productId);
        foreach($products as $product){ 
          $productPrice = $product["price"] * $productCount; ?>
          <p><?php echo "商品名: ".$product["name"];?></p>
          <p><?php echo "<img src='image/$product[image]' width='200' height='200'>";?></p>
          <p><?php echo "紹介文: ".$product["introduction"];?></p>
          <p><?php echo "商品金額: ".$product["price"]."円";?></p>
          <p><?php echo "現在の数量: ".$productCount."個";?></p>
          <p><?php echo "合計金額: ".$product["price"] * $productCount."円";?></p>
        <?php
          $ttlCartProducts += $productCount;
          $ttlCartPrice += $productPrice;
          $cartInProduct[] = $product["id"];
          $cartInCount[] = $productCount;
        }?>
      <p>------------------------------------------------------------</p>
    <?php
    }?>
    <p>▼カートの中身合計</p>
    <p><?php echo "合計:".$ttlCartProducts."点";?></p>
    <p><?php echo "合計金額:".$ttlCartPrice."円";?></p>
    <form action='' method='POST'>
      <p>▼配送先(※登録住所と異なる場合は入力してください)</p>
      <p>郵便番号</p>
      <input type="text" id="zipcode" value="" maxlength="7">
      <input type="button" id="search_btn" value="住所を検索">
      <div id="zip_result">
        <p>都道府県</p>
        <input type="text" name="pref">
        <p>市区町村</p>
        <input type="text" name="city">
        <p>町域</p>
        <input type="text" name="address">
      </div>
      <p>▼登録住所</p>
      <?php echo $address?>
      <br><br>
      <input type="checkbox" name="checkAddress" value="checkAddress">配送先を登録住所と同じにする
      <p>▼支払い方法</p>
      <select name="payment">
        <option value="">支払い方法を選択</option>
        <option value="1">銀行振り込み</option>
        <option value="2">着払い</option>
      </select>
      <p>------------------------------------------------------------</p>
      <?php for($i=0; $i < count($_SESSION["cart"]); $i++){ ?>
        <input type="hidden" name="productId[<?php echo $i;?>]" value="<?php echo $cartInProduct[$i];?>">
        <input type="hidden" name="cartInCount[<?php echo $i;?>]" value="<?php echo $cartInCount[$i];?>">
      <?php } ?>
      <input type="hidden" name="ttlPrice" value="<?php echo $ttlCartPrice?>">
      <input type='submit' name="complete" value='確定する'>
    </form>
  <br>
  <input type="button" onclick="location.href='./cart.php'" value="戻る">
  
</body>
</html>