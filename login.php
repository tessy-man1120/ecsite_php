<?php
  session_start();
  require_once "validation/login_validation.php";
  $loginValidation->RedirectList();

  if(!empty($_SESSION["alertLogin"])){
    $alertMsg = $_SESSION["alertLogin"];
    unset($_SESSION["alertLogin"]);
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ログイン画面</title>
</head>
<body>
  <h1>ログイン画面</h1>
  <?php if(!empty($alertMsg)){?>
    <p>※ログインもしくは会員登録をしてください※</p>  
  <?php }?>
  <input type="button" onclick="location.href='./product_list.php'" value="ログインせずに商品を見る">
  <?php $loginValidation->LoginValidation();?>
  <form action="" method="POST">
    <p>メールアドレス</p>
    <input type="text" name="email">
    <p>パスワード</p>
    <input type="text" name="password">
    <br><br>
    <input type="submit" value="ログイン">
  </form>
  <br>
  <input type="button" onclick="location.href='./user_register.php'" value="会員登録画面へ">
</body>
</html>