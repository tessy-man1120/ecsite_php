<?php
  session_start();
  require_once "connect_database.php";
  require_once "validation/login_validation.php";
  $connectDb->ConnectDatabase($db);
  $loginValidation->RedirectLogin();

  if(empty($_SESSION["cart"])){
    header("Location: product_list.php");
  } 

  if(isset($_POST["delete"])){
    $number = $_POST["deleteProduct"];
    unset($_SESSION["cart"][$number]);
    $_SESSION["cart"] = array_merge($_SESSION["cart"]);
  }

  for($i=0; $i < count($_SESSION["cart"]); $i++){
    if(isset($_POST["edit$i"])){
        $_SESSION["cart"][$i]["count"] = $_POST["countProducts"][$i];
      }
  }

  if(isset($_POST["deleteAll"])){
    unset($_SESSION["cart"]);
  }
  if(isset($_POST["editAll"])){
    for($i=0; $i < count($_SESSION["cart"]); $i++){
      $_SESSION["cart"][$i]["count"] = $_POST["countProducts"][$i];
    }
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>カート一覧画面</title>
</head>
<body>
  <h1>カート一覧画面</h1>
  <?php
    if(!empty($_SESSION["cart"])){
      $ttlCartProducts = 0;
      $ttlCartPrice = 0;?>
      <form action="" method="POST" id="edit"></form>
      <?php
      foreach($_SESSION["cart"] as $index => $product){
        $productId = $product["id"];
        $productCount = $product["count"];
        $connectDb->SelectCartProducts($db, $products, $productId);
          foreach($products as $product){ 
            $productPrice = $product["price"] * $productCount; ?>
            <p><?php echo "商品名: ".$product["name"];?></p>
            <p><?php echo "<img src='image/$product[image]' width='200' height='200'>";?></p>
            <p><?php echo "紹介文: ".$product["introduction"];?></p>
            <p><?php echo "商品金額: ".$product["price"]."円";?></p>
            <p><?php echo "現在の数量: ".$productCount."個";?></p>
            <p><?php echo "合計金額: ".$product["price"] * $productCount."円";?></p>
              ➡︎
              <input form="edit" type="number" name="countProducts[<?php echo $index;?>]" value=<?php echo $productCount;?> min="1">
              個へ
              <input form="edit" type="submit" name="edit<?php echo $index;?>" value="変更">
            <form action="" method="POST" id="delete<?php echo $index;?>">
              <input form="delete<?php echo $index;?>" type="hidden" name="deleteProduct" value=<?php echo $index;?>>
              <input form="delete<?php echo $index;?>" type="submit" name="delete" value="削除">
            </form>
            <?php
              $ttlCartProducts += $productCount;
              $ttlCartPrice += $productPrice;
            ?>
          <?php } ?>
        <p>------------------------------------------------------------</p>
        <?php
      }?>

      <h4>▼カートの中身合計</h4>
        <p><?php echo "合計:".$ttlCartProducts."点";?></p>
        <p><?php echo "合計金額:".$ttlCartPrice."円";?></p>
        <input form="edit" type="submit" name="editAll" value="全ての個数を変更する">
        <input form="edit" type="submit" name="deleteAll" value="カートを空にする">
      <p>------------------------------------------------------------</p>
      <form action="order_confirm.php" method="POST">
        <input type="submit" name="confirm" value="購入手続きに進む">
      </form>
  <?php } else { ?>
    <h4>カートの中身は空です</h4>
  <?php } ?><br>
  <input type="button" onclick="location.href='./product_list.php'" value="商品一覧へ戻る">
</body>
</html