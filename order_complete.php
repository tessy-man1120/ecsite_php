<?php
  session_start();
  require_once "mail.php";
  require_once "connect_database.php";
  require_once "validation/login_validation.php";
  $loginValidation->RedirectLogin();
  $connectDb->ConnectDatabase($db);
  // $mailAutomatic->Mailer(); #自動メール送信機能

  if(empty($_SESSION["cart"])){
    header("Location: product_list.php");
  }

  // トランザクション処理
  try {
    $db->beginTransaction();
    $stmt = $db->prepare("INSERT INTO orders(id, user_id, address, total_price, payment)VALUES(NULL, :user_id, :address, :total_price, :payment)");
    $stmt->bindParam(':user_id', $_SESSION["loginId"], PDO::PARAM_STR);
    $stmt->bindParam(':address', $_SESSION["sendAddress"], PDO::PARAM_STR);
    $stmt->bindParam(':total_price', $_SESSION["ttlPrice"], PDO::PARAM_STR);
    $stmt->bindParam(':payment', $_SESSION["payment"], PDO::PARAM_STR);
    $stmt->execute();

    $orderId = $db->prepare("SELECT id FROM orders WHERE id = (select max(id) from orders)");
    $orderId->execute();
    foreach($orderId as $v){
      $orderId = $v["id"];
    }

    for($i=0; $i < count($_SESSION["cart"]); $i++){
      $stmt = $db->prepare("INSERT INTO order_detail(id, order_id, product_id, count)VALUES(NULL, :order_id, :product_id, :count)");
      $stmt->bindParam(':order_id', $orderId, PDO::PARAM_STR);
      $stmt->bindParam(':product_id', $_SESSION["productId"][$i], PDO::PARAM_STR);
      $stmt->bindParam(':count', $_SESSION["cartInCount"][$i], PDO::PARAM_STR);
      $stmt->execute();
    }

    $db->commit();

  } catch(PDOException $e) {
    $db->rollBack();
    echo "失敗しました" .$e->getMessage()."\n";
  }

  // ログイン情報以外のセッションを削除
  $id = $_SESSION["loginId"];
  $name = $_SESSION["loginName"];
  $mail = $_SESSION["userEmail"];
  $_SESSION = array();
  $_SESSION["loginId"] = $id;
  $_SESSION["loginName"] = $name;
  $_SESSION["userEmail"] = $mail;

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>購入完了</title>
</head>
<body>
  <h1>購入完了です！</h1>
  <h4>ご購入ありがとうございます</h4>
  <input type="button" onclick="location.href='./product_list.php'" value="商品一覧画面へ">
</body>
</html>
