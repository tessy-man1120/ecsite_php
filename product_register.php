<?php
  session_start();
  require_once "validation/product_validate.php";
  require_once "validation/login_validation.php";
  $loginValidation->PermitAdmin();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品登録</title>
</head>
<body>
  <h1>商品登録画面</h1>
  <?php
    $productValidation->ProductValidation();
  ?>
  <form action="" method="POST" enctype="multipart/form-data">
    <p><b>商品名</b></p>
    <input type="text" name="name">
    <p><b>商品紹介文</b></p>
    <textarea name="introduction"  cols="50" rows="10"></textarea>
    <p><b>商品価格</b> ※半角英数字</p>
    <input type="text" name="price">
    <p><b>商品画像</b></p>
    <input type="file" name="image"><br>
    <br><br>
    <input type="submit" value="送信" name="send">
  </form>
  <br>
  <input type="button" onclick="location.href='./product_list.php'" value="戻る">
</body>
</html>