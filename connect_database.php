<?php
// データベースと接続のクラス
  class ConnectDatabaseClass{

    // PDO接続
    function ConnectDatabase(&$db){
      $dsn = "mysql:dbname=ec_site;host=localhost;charaset=utf8";
      $user = "root";
      $password = "";
      try {
        $db = new PDO($dsn, $user, $password);
      } catch(PDOException $e) {
        echo "接続失敗" .$e->getMessage()."\n";
      }
      return $db;
    }


    // Users関連のDB接続
    function InsertUsers(&$db){
      $_SESSION["address"] = 
      $stmt = $db->prepare("INSERT INTO users(id, name, email, password, address, admin)VALUES(NULL, :name, :email, :password, :address, 1)");
      $stmt->bindParam(':name', $_SESSION["name"], PDO::PARAM_STR);
      $stmt->bindParam(':email', $_SESSION["email"], PDO::PARAM_STR);
      $pass = password_hash($_SESSION["password"], PASSWORD_DEFAULT);
      $stmt->bindParam(':password', $pass, PDO::PARAM_STR);
      $stmt->bindParam(':address', $_SESSION["addressAll"], PDO::PARAM_STR);
      $stmt->execute();
    }

    function SelectUserAddress(&$db, &$userAddress){
      $userAddress = $db->prepare("SELECT address FROM users WHERE id = $_SESSION[loginId]");
      $userAddress->execute();
      $userAddress = $userAddress->fetch();
      $userAddress = $userAddress[0];
      return $userAddress[0];

    }


    // ProductsのDB接続
    function InsertProducts(&$db){
      $stmt = $db->prepare("INSERT INTO products(id, name, image, introduction, price)VALUES(NULL, :name, :image, :introduction, :price)");
      $stmt->bindParam(':name', $_SESSION["name"], PDO::PARAM_STR);
      $stmt->bindParam(':image', $_SESSION["image"], PDO::PARAM_STR);
      $stmt->bindParam(':introduction', $_SESSION["introduction"], PDO::PARAM_STR);
      $stmt->bindParam(':price', $_SESSION["price"], PDO::PARAM_STR);
      $stmt->execute();
    }

    function SelectProductsAll(&$db, &$products){
      $products = $db->prepare("SELECT * FROM products");
      $products->execute();
    }

    function SelectProductDetail(&$db, &$products){
      $products = $db->prepare("SELECT * FROM products WHERE id = $_GET[id]");
      $products->execute();
    }

    function SelectCartProducts(&$db, &$products, $productId){
      $products = $db->prepare("SELECT * FROM products WHERE id = $productId");
      $products->execute();
    }
    // ranking機能のDB接続
    function CheckRankOrder(&$db, &$products){
      $products = $db->prepare("SELECT *, SUM(count) AS sum 
                                FROM products
                                INNER JOIN order_detail
                                ON products.id = order_detail.product_id
                                GROUP BY product_id 
                                ORDER BY sum DESC 
                              ");
      $products->execute();
    }
    // 新着商品表示機能
    function NewInformation(&$db, &$newProductId, &$newProductName){
      $inf = $db->prepare("SELECT id, name FROM products WHERE id = (select max(id) from products)");
      $inf->execute();
      foreach($inf as $v){
        $newProductId = $v["id"];
        $newProductName = $v["name"];
      }
    }

    // reviewsのDB接続
    function InsertReviews(&$db){
      $stmt = $db->prepare("INSERT INTO review(id, nickname, text, user_id, product_id)VALUES(NULL, :nickname, :text, :user_id, :product_id)");
      $stmt->bindParam(':nickname', $_POST["nickname"], PDO::PARAM_STR);
      $stmt->bindParam(':text', $_POST["review_text"], PDO::PARAM_STR);
      $stmt->bindParam(':user_id', $_SESSION["loginId"], PDO::PARAM_STR);
      $stmt->bindParam(':product_id', $_SESSION["productId"], PDO::PARAM_STR);
      $stmt->execute();
    }

    function SelectReviewsAll(&$db, &$product_review){
      $product_review = $db->prepare("SELECT * FROM review WHERE product_id = $_GET[id] ORDER BY id DESC");
      $product_review->execute();
    }

    function DeleteReview(&$db){
      $review = $db->prepare("DELETE FROM review WHERE id = $_POST[reviewId]");
      $review->execute();
    }

    // favoritesのDB接続
    function InsertFavorites(&$db){
      $stmt = $db->prepare("INSERT INTO favorites(id, user_id, product_id)VALUES(NULL, :user_id, :product_id)");
      $stmt->bindParam(':user_id', $_SESSION["loginId"], PDO::PARAM_STR);
      $stmt->bindParam(':product_id', $_GET["id"], PDO::PARAM_STR);
      $stmt->execute();
    }

    function DeleteFavorites(&$db){
      $review = $db->prepare("DELETE FROM favorites WHERE product_id = $_POST[productId]");
      $review->execute();
    }

    function CountFavorites(&$db, &$favoriteCount){
      $favorite = $db->prepare("SELECT id FROM favorites WHERE product_id = $_GET[id]");
      $favorite->execute();
      $favoriteCount = $favorite->rowCount();
    }

    function CheckFavoriteUser(&$db, &$check, &$checkFavorite){
      $check = $db->prepare("SELECT id FROM favorites WHERE product_id = $_GET[id]");
      $check->execute();
      $checkFavorite = $check->rowCount();      
    }

  }

  $connectDb = new ConnectDatabaseClass;

?>