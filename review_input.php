<?php
  session_start();
  require_once "validation/login_validation.php";
  require_once "connect_database.php";
  require_once "validation/review_validate.php";
  $loginValidation->RedirectLogin();
  $connectDb->ConnectDatabase($db);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>レビュー登録</title>
</head>

<body>
  <h1>レビューを登録する</h1>
  <?php $reviewValidation->ReviewValidation();?>
  <form action="" method="POST">
    <p>ニックネーム（50文字まで）</p>
    <input type="text" name="nickname">
    <br>
    <p>レビュー（140文字まで）</p>
    <textarea name="review_text"  cols="50" rows="10" ></textarea>
    <br><br>
    <input type="submit" name="review_input" value="送信する">
</form>
  <br>
  <input type="button" onclick="location.href='./product_detail.php?id=<?php echo $_SESSION['productId'];?>'" value="商品詳細へ戻る";>
</body>
</html>