<?php
  session_start();
  require_once "validation/login_validation.php";
  require_once "connect_database.php";
  $loginValidation->PermitAdmin();
  $connectDb->ConnectDatabase($db);
  $connectDb->InsertProducts($db);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>登録完了</title>
</head>
<body>
  <h1>登録完了画面</h1>
  <h3>商品登録完了です！</h3>
  <input type="button" onclick="location.href='./product_list.php'" value="商品一覧画面へ">
</body>
</html>