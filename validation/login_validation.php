<?php
  // ログイン関連のバリデーションとリダイレクト処理
  class LoginValidationClass{

    function LoginValidation(){
      $dsn = "mysql:dbname=ec_site;host=localhost;charaset=utf8";
      $user = "root";
      $password = "";
      try {
        $db = new PDO($dsn, $user, $password);
      } catch(PDOException $e) {
          echo "接続失敗" .$e->getMessage()."\n";
      } 
      $stmt = $db->prepare("SELECT * FROM users WHERE email = :email");
      $stmt->bindParam(':email', $_POST["email"], PDO::PARAM_STR);
      $stmt->execute();
      $userData = $stmt->fetch();

      if(isset($_POST["password"])){
        if(password_verify($_POST["password"], $userData["password"])){
          $_SESSION["loginId"] = $userData["id"];
          $_SESSION["loginName"] = $userData["name"];
          $_SESSION["userEmail"] = $userData["email"];
          if(($userData["admin"] == 0)){
            $_SESSION["adminId"] = "adminLogin";
          }
          header("Location: product_list.php");
        } else{
          $error = "※※メールアドレスもしくはパスワードが間違っています※※";
          echo "<b>$error</b>";
        }
      }
    }

    // ログイン時/未ログイン時のリダイレクト処理
    function RedirectList(){
      if(!empty($_SESSION["loginId"])){
        header("Location: product_list.php");
      }  
    }
    function RedirectLogin(){
      if(empty($_SESSION["loginId"])){
        header("Location: login.php");
      }  
    }
    function PermitAdmin(){
      if(empty($_SESSION["adminId"])){
        header("Location: login.php");
      }
    }

  }

  $loginValidation = new LoginValidationClass;

?>