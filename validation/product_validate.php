<?php
  // 商品登録のバリデーション
  class ProductValidationClass {

    function ProductValidation(){
      $error = NULL;
      if(isset($_POST["send"])){
        $tempFile = $_FILES["image"]["tmp_name"];
        $fileMove = "/Applications/XAMPP/xamppfiles/htdocs/ecsite_php/image/".$_FILES["image"]["name"];
        move_uploaded_file($tempFile, $fileMove);
        $_SESSION["image"] = $_FILES["image"]["name"];
        if(empty($_SESSION["image"])){
          $errors[] = "商品画像が挿入されていません";
        }
      }
      if(isset($_POST["name"])){
        $_SESSION["name"] = $_POST["name"];
        if(empty($_SESSION["name"])){
          $errors[] = "商品名が入力されていません";
        }
      }
      if(isset($_POST["introduction"])){
        $_SESSION["introduction"] = $_POST["introduction"];
        if(empty($_SESSION["introduction"])){
          $errors[] = "商品紹介文が登録されていません";
        }
      }
      if(isset($_POST["price"])){
        $_SESSION["price"] = $_POST["price"];
        if(empty($_SESSION["price"])){
          $errors[] = "商品価格が入力されていません";
        }
        if(!preg_match('/^0$|^-?[1-9][0-9]*$/', $_SESSION["price"])){
          $errors[] = "半角数字で入力してください";
        }
        if(empty($errors)){
          header("Location: product_confirm.php");
        }
      }
      if(!empty($errors)){
        echo "<ul>";
        foreach($errors as $error){
            echo "<li>"; 
            echo $error;
            echo "</li>"; 
        }
        echo "</ul>";
      }
    }
  }

  $productValidation = new ProductValidationClass;

?>