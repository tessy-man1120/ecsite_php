<?php
// 新規会員登録のバリデーションクラス
  class UserValidationClass{
    
    function RegisterValidation(){
      $error = NULL;
      if(isset($_POST["name"])){
        $_SESSION["name"] = $_POST["name"];
        if(empty($_SESSION["name"])){
          $errors[] = "名前が入力されていません";
        }
      }
      if(isset($_POST["email"])){
        $_SESSION["email"] = $_POST["email"];
        if(empty($_SESSION["email"])){
          $errors[] = "メールアドレスが入力されていません";
        }
        elseif(!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\?\*\[|\]%'=~^\{\}\/\+!#&\$\._-])*@([a-zA-Z0-9_-])+\.([a-zA-Z0-9\._-]+)+$/", $_POST["email"])){
          $errors[] = "メールアドレスを正しく入力ください";
        }
      }
      if(isset($_POST["password"])){
        $_SESSION["password"] = $_POST["password"];
        if(empty($_SESSION["password"])){
          $errors[] = "パスワードが入力されていません";
        }
      }
      if(isset($_POST["address"])){
        $_SESSION["addressAll"] = $_POST["pref"].$_POST["city"].$_POST["address"];
        if(empty($_POST["pref"])){
          $errors[] = "都道府県が入力されていません";
        }
        if(empty($_POST["city"])){
          $errors[] = "市区町村が入力されていません";
        }
        if(empty($_POST["address"])){
          $errors[] = "町域が入力されていません";
        }
        if(empty($errors)){
          header("Location: user_confirm.php");
        }
      }

      if(!empty($errors)){
        echo "<ul>";
        foreach($errors as $error){
            echo "<li>"; 
            echo $error;
            echo "</li>"; 
        }
        echo "</ul>";
      }
    }

  }

  $userValidation = new UserValidationClass;

?>