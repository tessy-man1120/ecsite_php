<?php
  // レビューのバリデーション
  class ReviewValidationClass {

    function ReviewValidation(){
      $error = NULL;
      if(isset($_POST["review_input"])){
        if(empty($_POST["nickname"])){
          $errors[] = "ニックネームを入力してください";
        }
        $nicknameLength = strlen($_POST["nickname"]);
        if (50 < $nicknameLength) {
        $errors[] = 'ニックネームは50文字以内で入力して下さい';
        }
        if(empty($_POST["review_text"])){
          $errors[] = "レビュー内容を入力してください";
        }
        $nicknameLength = strlen($_POST["review_text"]);
        if (140 < $nicknameLength) {
        $errors[] = 'レビューは140文字以内で入力して下さい';
        }
        if(empty($errors)){
          $dsn = "mysql:dbname=ec_site;host=localhost;charaset=utf8";
          $user = "root";
          $password = "";
          try {
            $db = new PDO($dsn, $user, $password);
          } catch(PDOException $e) {
            echo "接続失敗" .$e->getMessage()."\n";
          }
          // $connectDb->InsertReviews($db);
          
          $stmt = $db->prepare("INSERT INTO review(id, nickname, text, user_id, product_id)VALUES(NULL, :nickname, :text, :user_id, :product_id)");
          $stmt->bindParam(':nickname', $_POST["nickname"], PDO::PARAM_STR);
          $stmt->bindParam(':text', $_POST["review_text"], PDO::PARAM_STR);
          $stmt->bindParam(':user_id', $_SESSION["loginId"], PDO::PARAM_STR);
          $stmt->bindParam(':product_id', $_SESSION["productId"], PDO::PARAM_STR);
          $stmt->execute();
          header("Location: product_list.php");
        }
      }
      if(!empty($errors)){
        echo "<ul>";
        foreach($errors as $error){
            echo "<li>"; 
            echo $error;
            echo "</li>"; 
        }
        echo "</ul>";
      }
    }
  }

  $reviewValidation = new ReviewValidationClass;

?>