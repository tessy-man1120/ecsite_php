<?php
  session_start();
  require_once "validation/login_validation.php";
  $loginValidation->RedirectList();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>確認画面</title>
</head>
<body>
  <h1>確認画面</h1>
  <h2>こちらで登録しますか？</h2>
  <form action="user_complete.php" method="POST">
    <p><b>名前</b></p>
    <?php echo $_SESSION["name"]; ?>
    <p><b>メールアドレス</b></p>
    <?php echo $_SESSION["email"]; ?>
    <p><b>住所</b></p>
    <?php echo $_SESSION["addressAll"]; ?>
    <br><br>
    <input type="submit" value="登録">
  </form>
  <br>
  <input type="button" onclick="location.href='./user_register.php'" value="戻る">
</body>
</html>