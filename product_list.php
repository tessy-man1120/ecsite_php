<?php
  session_start();
  require_once "connect_database.php";
  require_once "validation/login_validation.php";
  // $loginValidation->RedirectLogin();
  $connectDb->ConnectDatabase($db);
  $connectDb->SelectProductsAll($db, $products);
  $connectDb->NewInformation($db, $newProductId, $newProductName);

  if(isset($_POST["logout"])){
    $_SESSION = array();
    session_destroy();
    header("Location: login.php");
  }
  
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品一覧</title>
</head>
<body>
  <h1>商品一覧画面</h1>
  <?php if(empty($_SESSION["loginId"])){?>
    <p>ゲストさん</p>
    <input type="button" onclick="location.href='./user_register.php'" value="会員登録する";>
    <br><br>
  <?php }else{?>
    <p><?php echo $_SESSION["loginName"];?>さんでログイン中</p>
  <?php }?>
  <!-- ログイン時のみの処理「ログアウト」「カートボタン」 -->
  <?php if(!empty($_SESSION["loginId"])){?>
    <form action="" method="POST">
      <input type="submit" name="logout" value="ログアウト" style="background-color:red;color:white";>
    </form>
    <br>
    <?php
      if(empty($_SESSION["cart"])){
        echo "<p>※現在カートの中身は空です※</p>";
      }
    ?>
    <form action="cart.php" method="POST">
      <input type="submit" value="カートの中身を確認する" style="width:200px;height:50px">
    </form>
    <br>
  <?php }?>

  <input type="button" onclick="location.href='./top_order.php'" value="人気商品を見る" style="width:200px;height:50px;background-color:blue;color:white";>
  <br>
  <p>-----------------------------------------------------------------------------------------</p>
  <h3 style="color:red;">★新着商品情報★</h3>
  <?php echo "<a style=font-weight:bold; href=product_detail.php?id=" . $newProductId .">商品名:$newProductName</a>";?>
  <p>-----------------------------------------------------------------------------------------</p>
  <br>
    <?php if(isset($_SESSION["adminId"])){ ?>
    <form action="product_register.php" method="POST">
      <input style="width:200px;height:50px" type="submit" name="productReg" value="商品登録画面へ">
    </form>
    <?php } ?>
  <br>

  <table style="margin-bottom:100px;" border="1" align="left">
    <tr>
      <th width="150">商品名</th>
      <th width="200">画像</th>
      <th width="250">紹介文</th>
      <th width="150">価格</th>
      <th width="100"></th>
    </tr>
    <?php foreach($products as $product){ 
      ?>
      <tr>
        <td><?php echo "$product[name]"; ?></td>
        <td><?php echo "<img src='image/$product[image]' width='150' height='150'>" ?></td>
        <td><?php echo "$product[introduction]"; ?></td>
        <td><?php echo "$product[price] 円"; ?></td>
        <td>
        <?php echo "<a href=product_detail.php?id=" . $product["id"] . ">詳細</a>";?>
        </td>
      </tr>
    <?php } ?>
    </table>
</body>
</html>