<?php
require_once "connect_database.php";
$connectDb->ConnectDatabase($db);
$connectDb->CheckRankOrder($db ,$products);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>売れ筋ランキング</title>
</head>
<body>
  <h1>売れ筋商品（1位〜5位）</h1>
    <?php if(isset($_SESSION["adminId"])){ ?>
    <form action="product_register.php" method="POST">
      <input type="submit" name="productReg" value="商品登録画面へ">
    </form>
    <?php } ?>
  <br>
  <input type="button" onclick="location.href='./product_list.php'" value="商品一覧へ戻る">
  <br><br>
  <table style="margin-bottom:50px;" border="1" align="left">
    <tr>
      <th width="50">順位</th>
      <th width="150">商品名</th>
      <th width="200">画像</th>
      <th width="250">紹介文</th>
      <th width="150">価格</th>
      <th width="100"></th>
    </tr>
    <?php $rank = 1;?>
    <?php foreach($products as $product){ 
      ?>
      <tr>
        <td><?php echo $rank; ?></td>
        <td><?php echo "$product[name]"; ?></td>
        <td><?php echo "<img src='image/$product[image]' width='150' height='150'>" ?></td>
        <td><?php echo "$product[introduction]"; ?></td>
        <td><?php echo "$product[price] 円"; ?></td>
        <td>
        <?php echo "<a href=product_detail.php?id=" . $product["product_id"] . ">詳細</a>";?>
        </td>
      </tr>
      <?php $rank +=1; ?>
    <?php } ?>
    </table>
</body>
</html>