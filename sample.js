$(function () {
  $("#search_btn").click(function () {
      var param = {zipcode: $('#zipcode').val()}
      var send_url = "http://zipcloud.ibsnet.co.jp/api/search";
      $.ajax({
          type: "GET",
          cache: false,
          data: param,
          url: send_url,
          dataType: "jsonp",
          success: function (res) {
              if (res.status == 200) {
                  var html = '';
                  for (var i = 0; i < res.results.length; i++) {
                      var result = res.results[i];
                      html += '<p>都道府県</p>' + '<input type="text" name="pref" value=' + result.address1 + '>';
                      html += '<p>市区町村</p>' + '<input type="text" name="city" value=' + result.address2 + '>';
                      html += '<p>町域</p>' + '<input type="text" name="address" value=' + result.address3 + '>';
                  }
                  $('#zip_result').html(html);
              } else {
                  $('#zip_result').html(res.message);
              }
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
          }
      });
  });
});