<?php
  session_start();
  require_once "connect_database.php";
  require_once "validation/login_validation.php";
  $loginValidation->RedirectList();
  $connectDb->ConnectDatabase($db);
  $connectDb->InsertUsers($db);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>完了</title>
</head>
<body>
  <h1>完了画面</h1>
  <h2>会員登録完了しました</h2>
  <input type="button" onclick="location.href='./login.php'" value="ログイン画面へ">
  <!-- <input type="button" onclick="location.href='./product_list.php'" value="商品一覧画面へ"> -->
</body>
</html>