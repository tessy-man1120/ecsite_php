<?php
  session_start();
  require_once "validation/login_validation.php";
  require_once "connect_database.php";
  // $loginValidation->RedirectLogin();
  $connectDb->ConnectDatabase($db);
  $connectDb->SelectProductDetail($db, $products);
  $connectDb->SelectReviewsAll($db, $product_review);
    if(empty($_SESSION["cart"])){
      $_SESSION["cart"] = [];
    }
    $match = array_search($_GET["id"], array_column($_SESSION["cart"], "id"));
    if(is_numeric($match)){
      $orderedCount = $_SESSION["cart"][$match]["count"];
    }
    if(isset($_POST["cartIn"])){
      if(is_numeric($match)){
        $_SESSION["cart"][$match] = ["id"=> $_GET["id"] ,"count" => $_POST["count"]];
      }else{
        $_SESSION['cart'][] = ["id" => $_GET["id"] ,"count" => $_POST['count']];
      }
      header("Location: product_list.php");
    }

    if(isset($_POST["review"])){
      if(empty($_SESSION["loginId"])){
        $_SESSION["alertLogin"] = "※ログインもしくは会員登録をしてください※";
        header("Location: login.php");
      }
      $_SESSION["productId"] = $_GET["id"];
      header("Location: review_input.php");
    }

    if(isset($_POST["review_delete"])){
      $connectDb->DeleteReview($db);
      header("Location: delete_complete.php");
    }

    if(isset($_POST["upLike"])){
      if(empty($_SESSION["loginId"])){
        header("Location: login.php");
      }
      $connectDb->InsertFavorites($db);
    }
    if(isset($_POST["downLike"])){
      $connectDb->DeleteFavorites($db);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品詳細画面</title>
</head>
<body>
  <?php foreach($products as $product){ ?>
    <h1>商品詳細</h1>
    <p><b>商品名</b></p>
    <?php echo "$product[name]"; ?>
    <p><b>画像</b></p>
    <?php echo "<img src='image/$product[image]' width='200' height='200'>"; ?>
    <p><b>紹介文</b></p>
    <?php echo "$product[introduction]"; ?>
    <p><b>価格</b></p>
    <?php echo "$product[price] 円"; ?>
  <?php } ?><br><br>
  <?php if(!empty($_SESSION["loginId"])){?>
    <p><b>個数</b></p>
    <?php if(!empty($orderedCount)){
            echo "※現在のカート内の個数：".$orderedCount."個";
            }
    ?>
    <form action="" method="POST">
      <input type="hidden" name="productId" value='<?php echo "$_GET[id]"?>'>
      <input type="number" name="count" value=
      <?php if(empty($orderedCount)){
              echo 1;
            }else{
              echo $orderedCount;
            }?>
      min="1">
      個
      <br><br>
      <input style="width:150px;height:50px;" type="submit" name="cartIn" value="カートに入れる">
    </form><br>
  <?php }?>
  <input style="width:150px;height:25px;" type="button" onclick="location.href='./product_list.php'" value="商品一覧へ戻る">
  <br>
  <p>------------------------------------------------------------------------------------------</p>
  <?php $connectDb->CountFavorites($db, $favoriteCount);?>
  <p>いいねの数：❤︎<?php echo $favoriteCount;?></p>
  <form action="" method="POST">
    <input type="hidden" name="productId" value='<?php echo "$_GET[id]"?>'>
    <?php $connectDb->CheckFavoriteUser($db, $check, $checkFavorite);?>
    <?php if(isset($_SESSION["loginId"]) && $checkFavorite == 0){ ?>
      <input style="background-color:pink;color:white;" type="submit" name="upLike" value="いいねをする">
    <?php }elseif(isset($_SESSION["loginId"]) && $checkFavorite != 0){ ?>
      <input style="background-color:black;color:white;" type="submit" name="downLike" value="いいねを取り消す">
    <?php } ?>
    <br>
  </form>
  <p>------------------------------------------------------------------------------------------</p>
  <h3>商品のレビュー</h3>
  <?php $reviewZero = 0; ?>
    <table style="margin-bottom:50px;" border="1">
      <tr>
        <th width="200">ニックネーム</th>
        <th width="800">レビュー内容</th>
      </tr>
      <?php foreach($product_review as $review){ ?>
        <tr>
          <td><?php echo $review["nickname"]; ?>さん</td>
          <td><?php echo $review["text"]; ?></td>
          <?php if(isset($_SESSION["loginId"]) && $review["user_id"] == $_SESSION["loginId"]){ ?>
          <form action="" method="POST">
            <input type="hidden" name="reviewId" value="<?php echo $review['id']?>">
            <td><input type="submit" name="review_delete" value="削除"></td>
          </form>
        </tr>
        <?php } ?>
      <?php } ?>
    </table>
  <form action="" method="POST">
    <input type="hidden" name="productId" value='<?php echo "$_GET[id]"?>'>
    <input style="width:150px;height:50px;" type="submit" name="review" value="レビューを登録画面へ">
  </form>
</body>
</html>