<?php
  session_start();
  require_once "validation/login_validation.php";
  $loginValidation->PermitAdmin();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>確認画面</title>
</head>
<body>
    <h1>商品確認画面</h1>
    <h2>こちらで登録しますか？</h2>
    <form action="product_complete.php" method="POST">
      <p><b>商品名</b></p>
      <?php echo $_SESSION["name"]; ?>
      <p><b>商品画像</b></p>
      <?php echo "<img src='image/$_SESSION[image]' width='300' height='300'>" ?>
      <p><b>商品紹介文</b></p>
      <?php echo $_SESSION["introduction"]; ?>
      <p><b>商品価格</b></p>
      <?php echo $_SESSION["price"].' 円'; ?>
      <br><br>
      <input type="submit" value="登録">
    </form>
    <br>
  <input type="button" onclick="location.href='./product_register.php'" value="戻る">
</body>
</html>