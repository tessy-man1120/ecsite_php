<?php
  session_start();
  require_once "validation/user_validate.php";
  require_once "validation/login_validation.php";
  $loginValidation->RedirectList();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="sample.js"></script>
  <title>新規登録</title>
</head>
<body>
  <h1>会員登録画面</h1>
  <input type="button" onclick="location.href='./product_list.php'" value="会員登録せずに商品を見る">
  <?php
    $userValidation->RegisterValidation();
  ?>
  <form action="" method="POST">
    <p>名前</p>
    <input type="text" name="name">
    <p>メールアドレス</p>
    <input type="text" name="email">
    <p>パスワード</p>
    <input type="text" name="password">
    <p>郵便番号</p>
    <input type="text" id="zipcode" value="" maxlength="7">
    <input type="button" id="search_btn" value="住所を検索">
      <div id="zip_result">
        <p>都道府県</p>
        <input type="text" name="pref">
        <p>市区町村</p>
        <input type="text" name="city">
        <p>町域</p>
        <input type="text" name="address">
      </div>
      <br>
    <input type="submit" value="送信">
  </form>
  <br>
  <input type="button" onclick="location.href='./login.php'" value="ログイン画面へ">
</body>
</html>